﻿function GetCountry(_regionID) {
    var procemessage = "<option value='0'> Please wait...</option>";
    $("#ddlCountry").html(procemessage).show();
    var url = "/Home/GetCountriesByRegion/";

    $.ajax({
        url: url,
        data: { regionID: _regionID },
        cache: false,
        type: "POST",
        success: function (data) {
            var markup = "<option value='0'>Select Country</option>";
            for (var x = 0; x < data.length; x++) {
                markup += "<option value=" + data[x].Value + ">" + data[x].Text + "</option>";
            }
            $("#ddlCountry").html(markup).show();
        },
        error: function (reponse) {
            $("#ddlCountry").hide();
            $('#errorMessage').text(reponse.statusText);
        }
    });

}

function ChangeCountry(_country)
{
    var url = "/Home/ChangeCountry/";

    $.ajax({
        url: url,
        data: { countryId: _country.value, countryName: _country.selectedOptions["0"].innerHTML },
        cache: false,
        type: "POST",
        success: function (data)
        {
            window.location.reload(true);
        },
        error: function (data)
        {
            var dat = data;
        }
    });
}


$('#showDdlRegion').on('click', function (e) {
    e.preventDefault();
    $dropdown = $('#ddlRegions');
    $dropdown.parent().removeClass("hidden");
});