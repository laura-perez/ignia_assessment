﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IgniaWeather.Models
{
    public class Country
    {
        public string Id { get; set; }

        public string LocalizedName { get; set; }

        public string EnglishName { get; set; }

        //Default country Australia
        public Country()
        {
            this.Id = "AU";
            this.LocalizedName = "Australia";
            this.EnglishName = "Australia";
        }

        public Country(string id, string name)
        {
            this.Id = id;
            this.EnglishName = name;
        }
    }
}