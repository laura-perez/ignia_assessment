﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IgniaWeather.Models
{
    public class Region
    {
        public string ID { get; set; }
        public string LocalizedName { get; set; }
        public string EnglishName { get; set; }

        //default region
        public Region()
        {
            this.ID = "OCN";
            this.EnglishName = "Oceania";
            this.LocalizedName = "Oceania";
        }
    }
}