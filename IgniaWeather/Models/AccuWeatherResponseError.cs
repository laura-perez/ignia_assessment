﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IgniaWeather.Models
{
    public class AccuWeatherResponseError
    {
            public string Code { get; set; }
            public string Message { get; set; }
            public string Reference { get; set; }
    }
}