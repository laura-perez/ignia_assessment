﻿using IgniaWeather.Controllers.Services;
using IgniaWeather.DAL;
using IgniaWeather.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace IgniaWeather
{
    public class Global : System.Web.HttpApplication
    {
        public static AWServiceContext Services { get; set; }

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            log4net.Config.XmlConfigurator.Configure();
            
            //Instanciate Service context to use throughtout the application lifecycle
            Services = new AWServiceContext
            {
                AWService = new AccuWeather()
            };
        }

    }
}
