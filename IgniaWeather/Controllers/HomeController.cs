﻿using IgniaWeather.DAL;
using IgniaWeather.Models;
using IgniaWeather.ViewModels;
using System;
using System.Web;
using System.Web.Handlers;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace IgniaWeather.Controllers
{
    public class HomeController : Controller
    {

        public ActionResult Index()
        {
            var model = getSessionModel();

            if (ControllerContext.HttpContext.Session["firstVisit"] == null)
            {
                //check if client is known
                using (var dbContext = new IgniaWeatherContext())
                {
                    var clientIpAddress = this.ControllerContext.HttpContext.Request.UserHostAddress;
                    dbContext.Database.Connection.Open();

                    var usrloc = (from ul in dbContext.UserLocation where ul.IpAddress == clientIpAddress select ul).ToList().FirstOrDefault();

                    if (usrloc != null && !String.IsNullOrEmpty(usrloc.LocationKey))
                    {
                        try
                        { 
                            var location = Global.Services.AWService.getLocationByKey(usrloc.LocationKey);
                            model.currentLocation = location ?? location;
                            var weather = Global.Services.AWService.getCurrentConditions(usrloc.LocationKey);
                            model.weather = weather ?? weather;
                        }
                        catch (UnauthorizedAccessException ex)
                        {
                            model.ErrorMessage = ex.Message;
                        }
                    }

                    dbContext.Database.Connection.Close();
                }
                ControllerContext.HttpContext.Session["firstVisit"] = "set";
            }

            return View("Index", model);
        }

        public ActionResult About()
        {
            ViewBag.Message = "About this application";

            return View("About");
        }

        public ActionResult Search(HomeViewModel model)
        {
            if (model.currentLocation == null)
            {
                model = getSessionModel();
            }

            try { 
                if (!String.IsNullOrEmpty(Request.Params["term"]))
                {
                    var searchedLocation = Global.Services.AWService.getLocationAndDbSave(model.currentLocation.Country.Id, Request.Params["term"].ToString());

                    if (searchedLocation != null)
                    {
                        model.currentLocation = searchedLocation;

                        CurrentConditions currentWeather = Global.Services.AWService.getCurrentConditions(searchedLocation.Key);

                        model.weather = currentWeather;

                        ControllerContext.HttpContext.Session["homeViewModel"] = model;
                    }
                    else
                    {
                        model.ErrorMessage = "Location not found. Please try again";
                    }

                }
            }
            catch (UnauthorizedAccessException ex)
            {
                model.ErrorMessage = ex.Message;
            }

            return View("Index", model);
        }

        [HttpPost]
        public ActionResult GetCountriesByRegion(string regionID)
        {
            try
            {
                List<Country> countries = new List<Country>();
                if (!String.IsNullOrEmpty(regionID))
                {
                    countries = Global.Services.AWService.getCountryListByRegionId(regionID);
                    SelectList selectCountriesList = new SelectList(countries, "Id", "EnglishName", 0);
                    return Json(selectCountriesList);
                }
            }
            catch (UnauthorizedAccessException ex)
            {
                return new HttpStatusCodeResult(503, ex.Message);
            }

            return null;
        }

        public ActionResult ChangeCountry(string countryId, string countryName)
        {
            ModelState.Clear();
            var model = getSessionModel();
            if (!String.IsNullOrEmpty(countryId))
            {
                model.currentLocation.Country = new Country(countryId, countryName);
                ControllerContext.HttpContext.Session["homeViewModel"] = model;
            }

            return View("Index", model);
        }

        private HomeViewModel getSessionModel()
        {
            var model = new HomeViewModel();

            if (ControllerContext.HttpContext.Session["homeViewModel"] == null)
            {
                try { 
                    model.AllRegions = Global.Services.AWService.getAllRegions();
                }
                catch (UnauthorizedAccessException ex)
                {
                    model.ErrorMessage = ex.Message;
                }

                model.currentLocation = new Location
                {
                    Country = new Country(),
                    Region = new Region()
                };

                ControllerContext.HttpContext.Session["homeViewModel"] = model;
            }
            else
            {
                model = (HomeViewModel)ControllerContext.HttpContext.Session["homeViewModel"];
            }

            return model;
        }





    }
}