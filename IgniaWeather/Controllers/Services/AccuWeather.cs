﻿using IgniaWeather.DAL;
using IgniaWeather.Models;
using log4net;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Configuration;

namespace IgniaWeather.Controllers.Services
{
    public class AWServiceContext
    {
        public AccuWeather AWService { get; set; }
    }

    public class AccuWeather
    {
        private static string ApiKey = WebConfigurationManager.AppSettings["ApiKey"];
        private static ILog log = LogManager.GetLogger(typeof(AccuWeather));

        private static string callAccuWeatherAPi(WebRequest request)
        {
            string jsonResult = "";
            try
            {
                WebResponse response = request.GetResponse();
                using (Stream responseStream = response.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(responseStream, Encoding.UTF8);
                    jsonResult = reader.ReadToEnd();
                }
            }
            catch (WebException ex)
            {

                WebResponse errorResponse = ex.Response;
                using (Stream responseStream = errorResponse.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(responseStream, Encoding.GetEncoding("utf-8"));
                    String errorText = reader.ReadToEnd();
                    var errObject = JsonConvert.DeserializeObject<AccuWeatherResponseError>(errorText);

                    if (errObject.Code.Equals("ServiceUnavailable"))
                    {
                        throw new UnauthorizedAccessException(errObject.Message);
                    }

                    log.Error(ex.Message, ex);
                }
            }

            return jsonResult;
        }

        public CurrentConditions getCurrentConditions(string locationKey)
        {
            ////TODO: replace with API call
            //using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/App_Data/JsonData/sydneyWeather.json")))
            //{
            //    string json = reader.ReadToEnd();
            //    var deserializedLocation = JsonConvert.DeserializeObject<List<CurrentConditions>>(json);
            //    if (deserializedLocation.Count > 0)
            //    {
            //        weather = deserializedLocation.FirstOrDefault();
            //    }
            //}

            var host = "http://dataservice.accuweather.com";
            string APIUrl = host + "/currentconditions/v1/" + locationKey + "?apikey=" + ApiKey;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(APIUrl);

            try
            {
                string jsonResult = callAccuWeatherAPi(request);

                var deserializedLocation = JsonConvert.DeserializeObject<List<CurrentConditions>>(jsonResult);
                if (deserializedLocation.Count > 0)
                {
                    return deserializedLocation.FirstOrDefault();
                }
            }
            catch (UnauthorizedAccessException ex)
            {
                throw ex; //send it to controller for message processing
            }
            catch (Exception ex)
            {
                log.Error(ex.Message, ex);
            }

            return null;
        }

        #region Location
        /// <summary>
        /// Regions are listed in JSON file instead to limit accuweather API calls 
        /// </summary>
        /// <returns></returns>
        public List<Region> getAllRegions()
        {
            List<Region> regions = new List<Region>();

            //default region Oceania
            regions.Add(new Region());

            try
            {
                //TODO: Replace this code by API call 
                using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/App_Data/JsonData/regions.json")))
                {
                    string json = reader.ReadToEnd();
                    regions = JsonConvert.DeserializeObject<List<Region>>(json);
                }
            }
            catch (UnauthorizedAccessException ex)
            {
                throw ex; //send it to controller for message processing
            }
            catch (Exception exc)
            {
                log.Error(exc.Message, exc);
            }

            return regions;
        }

        public List<Country> getCountryListByRegionId(string regionID)
        {
            List<Country> countries = new List<Country>();
            countries.Add(new Country()); //add default

            ////HACK: get from json file
            //using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/App_Data/JsonData/countries.json")))
            //{
            //    string json = reader.ReadToEnd();
            //    countries = JsonConvert.DeserializeObject<List<Country>>(json);
            //}

            var host = "http://dataservice.accuweather.com";
            string APIUrl = host + "/locations/v1/countries/" + regionID + "?apikey=" + ApiKey;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(APIUrl);

            try
            {
                var jsonResult = callAccuWeatherAPi(request);
                countries = JsonConvert.DeserializeObject<List<Country>>(jsonResult);

            }
            catch (UnauthorizedAccessException ex)
            {
                throw ex; //send it to controller for message processing
            }
            catch (Exception ex)
            {
                log.Error(ex.Message, ex);
            }

            return countries;
        }

        public Location getLocationAndDbSave(string countryId, string searchTerm = null)
        {
            try
            {
                var location = getLocationByCountryWSeach(searchTerm, countryId);

                if (location != null)
                {
                    using (var dbContext = new IgniaWeatherContext())
                    {
                        var clientIpAddress = HttpContext.Current.Request.UserHostAddress;
                        dbContext.Database.Connection.Open();
                        if (dbContext.UserLocation.Where(x => x.IpAddress.Equals(clientIpAddress)).Count() > 0)
                        {
                            dbContext.UserLocation.Where(x => x.IpAddress.Equals(clientIpAddress)).FirstOrDefault().LocationKey = location.Key;
                        }
                        else
                        {
                            dbContext.UserLocation.Add(new UserLocation { IpAddress = clientIpAddress, LocationKey = location.Key });
                        }
                        dbContext.SaveChanges();
                    }
                }

                return location;
            }
            catch (UnauthorizedAccessException ex)
            {
                throw ex; //send it to controller for message processing
            }
            catch (Exception ex)
            {
                log.Error("getlocation error", ex);
            }

            return null;
        }

        private Location getLocationByCountryWSeach(string searchTerm, string countryId)
        {
            ////HACK: get data from json file
            //using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/App_Data/JsonData/perth.json")))
            //{
            //    string json = reader.ReadToEnd();
            //    var deserializedLocation = JsonConvert.DeserializeObject<List<Location>>(json);
            //    if (deserializedLocation.Count > 0)
            //    {
            //        location = deserializedLocation.FirstOrDefault();
            //    }
            //}

            var host = "http://dataservice.accuweather.com";
            string APIUrl = host + "/locations/v1/cities/" + countryId + "/search?apikey=" + ApiKey + "&q=" + searchTerm;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(APIUrl);

            try
            {
                var jsonResult = callAccuWeatherAPi(request);

                var deserializedLocation = JsonConvert.DeserializeObject<List<Location>>(jsonResult);
                if (deserializedLocation.Count > 0)
                {
                    return deserializedLocation.FirstOrDefault();
                }
            }
            catch (UnauthorizedAccessException ex)
            {
                throw ex; //send it to controller for message processing
            }
            catch (Exception ex)
            {
                log.Error(ex.Message, ex);
            }

            return null;
        }

        public Location getLocationByKey(string locationKey)
        {
            var host = "http://dataservice.accuweather.com";
            string APIUrl = host + "/locations/v1/" + locationKey + "?apikey=" + ApiKey;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(APIUrl);

            try
            {
                var jsonResult = callAccuWeatherAPi(request);

                return JsonConvert.DeserializeObject<Location>(jsonResult);

            }
            catch (UnauthorizedAccessException ex)
            {
                throw ex; //send it to controller for message processing
            }
            catch (Exception ex)
            {
                log.Error(ex.Message, ex);
            }

            return null;
        }
        #endregion
    }
}