﻿using IgniaWeather.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace IgniaWeather.DAL
{
    public class IgniaWeatherContext : DbContext
    {
        public IgniaWeatherContext() : base("IgniaWeatherContext")
        {
            
        }

        public DbSet<UserLocation> UserLocation { get; set; }

        //protected override void OnModelCreating(DbModelBuilder modelBuilder)
        //{
        //    Database.SetInitializer<IgniaWeatherContext>(null);
        //    base.OnModelCreating(modelBuilder);
        //}
    }

    
}