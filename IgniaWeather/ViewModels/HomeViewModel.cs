﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using IgniaWeather.Models;

namespace IgniaWeather.ViewModels
{
    public class HomeViewModel
    {
        public CurrentConditions weather { get; set; }

        public Location currentLocation { get; set; }

        public List<Region> AllRegions { get; set; }

        public List<Country> CountriesPerRegion { get; set; }

        public string ErrorMessage { get; set; }
    }
}