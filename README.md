# README #

### What is this repository for? ###

* Quick summary:

You are developing a website to display weather information. You want to allow users to select their location and show current weather information.

You've decided to use http://developer.accuweather.com REST API to acquire your data for the application. Create an account to obtain an API key to use against the APIs.

Users should be able to narrow their location search by country. Once the location is selected, ensure it is saved to a database for subsequent visits.

After the user has selected their location, display the current weather information via the following API:

http://developer.accuweather.com/accuweather-current-conditions-api/apis/get/currentconditions/v1/?locationKey

The user interface should work across a variety of devices and platforms including PCs, tablets and mobile phones.


### How do I get set up? ###

* Open solution file in Visual Studio and build to retrieve packages
* Setup connection string to connect a local database (please ensure to set the DB server security to SQL Server Authentication).
  Create a new database called "IgniaWeather". Please ensure your login has full permissions on this database (dbowner)
* It is very probable the number of calls per to AccuWeather API has been reached. Either use another key (in web.config), or uncomment //HACK in Controllers/API/AccuWeather.cs . This will read json data from files stored in App_Data/JsonData

### Who do I talk to? ###

* laura.perez.fr@gmail.com